package com.demo.installsoftware;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SyncStats;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import java.io.File;
import java.nio.file.spi.FileSystemProvider;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * InstallSoftwarePlugin
 */
public class InstallSoftwarePlugin implements MethodCallHandler, PluginRegistry.RequestPermissionsResultListener, PluginRegistry.ActivityResultListener {
    /**
     * Plugin registration.
     */

    private Context context;
    private Activity activity;

    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "install_software");
        InstallSoftwarePlugin installSoftwarePlugin = new InstallSoftwarePlugin(registrar.context(), registrar.activity());
        channel.setMethodCallHandler(installSoftwarePlugin);

        registrar.addRequestPermissionsResultListener(installSoftwarePlugin);
        registrar.addActivityResultListener(installSoftwarePlugin);
    }

    private InstallSoftwarePlugin(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        switch (call.method) {
            case "getPlatformVersion":
                result.success("Android " + Build.VERSION.RELEASE);
                break;
            case "install_apk":
                String path = call.argument("apk_path");
                if (path != null) {
                    Intent intent = new Intent();

                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                // 7.0+以上版本
//                assert path != null;
//                Uri apkUri =  FileProvider.getUriForFile(context, "com.xxxx.fileProvider", new File(path)); //与manifest中定义的provider中的authorities="com.xxxx.fileprovider"保持一致
//                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                        System.out.print("系统版本大于6.0");
                        result.success("系统版本大于6.0");
                    } else {
                        File file = new File(path);
                        System.out.print("生成文件结束");
                        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                        activity.startActivity(intent);
                    }
                } else {
                    result.success("找不到文件路径");
                }


//
//            assert path != null;
//            intent.setDataAndType(Uri.fromFile(new File(path)),
//                    "application/vnd.android.package-archive");
                break;
            default:
                result.notImplemented();
                break;
        }
    }

//    private void startActivity() {
//        File file = new File(filePath);
//        if (!file.exists()) {
//            result("the " + filePath + " file is not exists");
//            return;
//        }
//
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        intent.addCategory("android.intent.category.DEFAULT");
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            String packageName = context.getPackageName();
//            Uri uri = FileProvider.getUriForFile(context, packageName + ".fileProvider", new File(filePath));
//            intent.setDataAndType(uri, typeString);
//        } else {
//            intent.setDataAndType(Uri.fromFile(file), typeString);
//        }
//        activity.startActivity(intent);
//        result("done");
//    }


    @Override
    public boolean onActivityResult(int i, int i1, Intent intent) {
        return false;
    }

    @Override
    public boolean onRequestPermissionsResult(int i, String[] strings, int[] ints) {
        return false;
    }
}
